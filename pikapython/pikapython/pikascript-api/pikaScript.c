/*
 * [Warning!] This file is auto-generated by pika compiler.
 * Do not edit it manually.
 * The source code is *.pyi file.
 * More details: 
 * English Doc:
 * https://pikadoc-en.readthedocs.io/en/latest/PikaScript%20%E6%A8%A1%E5%9D%97%E6%A6%82%E8%BF%B0.html
 * Chinese Doc:
 * http://pikapython.com/doc/PikaScript%20%E6%A8%A1%E5%9D%97%E6%A6%82%E8%BF%B0.html
 */

#include "PikaMain.h"
#include <stdio.h>
#include <stdlib.h>

volatile PikaObj *__pikaMain;
PikaObj *pikaPythonInit(void){
    pika_platform_printf("======[pikapython packages installed]======\r\n");
    pika_printVersion();
    pika_platform_printf("PikaStdLib==85d49af7ce92733cf568e84a68177bfa6342ffa3\r\n");
    pika_platform_printf("_thread==bcc476ad430112f278b981d3416db6099bb94ac4\r\n");
    pika_platform_printf("aes==1edd8679e18aedeb44d3e3564979b9ac6eb1d216\r\n");
    pika_platform_printf("base64==1edd8679e18aedeb44d3e3564979b9ac6eb1d216\r\n");
    pika_platform_printf("json==f3a9b6fc43078779c11e0419edf8ad6e78e159bf\r\n");
    pika_platform_printf("math==85d49af7ce92733cf568e84a68177bfa6342ffa3\r\n");
    pika_platform_printf("random==3476cd603a668a200b19c384ed03daf98fa519be\r\n");
    pika_platform_printf("socket==85d49af7ce92733cf568e84a68177bfa6342ffa3\r\n");
    pika_platform_printf("time==bcc476ad430112f278b981d3416db6099bb94ac4\r\n");
    pika_platform_printf("===========================================\r\n");
    PikaObj* pikaMain = newRootObj("pikaMain", New_PikaMain);
    __pikaMain = pikaMain;
    extern unsigned char pikaModules_py_a[];
    obj_linkLibrary(pikaMain, pikaModules_py_a);
#if PIKA_INIT_STRING_ENABLE
    obj_run(pikaMain,
            "import PikaStdLib\n"
            "import math\n"
            "import socket\n"
            "import _thread\n"
            "import random\n"
            "import time\n"
            "import json\n"
            "import aes\n"
            "import base64\n"
            "import mqttclient\n"
            "\n");
#else 
    obj_runModule((PikaObj*)pikaMain, "main");
#endif
    return pikaMain;
}

