/*
 * [Warning!] This file is auto-generated by pika compiler.
 * Do not edit it manually.
 * The source code is *.pyi file.
 * More details: 
 * English Doc:
 * https://pikadoc-en.readthedocs.io/en/latest/PikaScript%20%E6%A8%A1%E5%9D%97%E6%A6%82%E8%BF%B0.html
 * Chinese Doc:
 * http://pikapython.com/doc/PikaScript%20%E6%A8%A1%E5%9D%97%E6%A6%82%E8%BF%B0.html
 */

#ifndef ___mqttclient__MQTT__H
#define ___mqttclient__MQTT__H
#include <stdio.h>
#include <stdlib.h>
#include "PikaObj.h"

PikaObj *New__mqttclient__MQTT(Args *args);

void _mqttclient__MQTT___del__(PikaObj *self);
void _mqttclient__MQTT___init__(PikaObj *self, char* ip, int port, char* clinetID, char* username, char* password, char* version, char* ca, int keepalive);
void _mqttclient__MQTT__fakeMsg(PikaObj *self, char* topic, int qos, char* msg);
int _mqttclient__MQTT_connect(PikaObj *self);
int _mqttclient__MQTT_disconnect(PikaObj *self);
PikaObj* _mqttclient__MQTT_listSubscribeTopic(PikaObj *self);
int _mqttclient__MQTT_publish(PikaObj *self, char* topic, char* payload, int qos);
int _mqttclient__MQTT_setCa(PikaObj *self, char* ca);
int _mqttclient__MQTT_setClientID(PikaObj *self, char* id);
int _mqttclient__MQTT_setDisconnectHandler(PikaObj *self, Arg* cb);
int _mqttclient__MQTT_setHost(PikaObj *self, char* host_url);
int _mqttclient__MQTT_setKeepAlive(PikaObj *self, int time);
int _mqttclient__MQTT_setPassword(PikaObj *self, char* passwd);
int _mqttclient__MQTT_setPort(PikaObj *self, int port);
int _mqttclient__MQTT_setUsername(PikaObj *self, char* name);
int _mqttclient__MQTT_setVersion(PikaObj *self, char* version);
int _mqttclient__MQTT_setWill(PikaObj *self, char* topic, char* payload, int qos, int retain);
int _mqttclient__MQTT_subscribe(PikaObj *self, char* topic, Arg* cb, int qos);
int _mqttclient__MQTT_unsubscribe(PikaObj *self, char* topic);

#endif
