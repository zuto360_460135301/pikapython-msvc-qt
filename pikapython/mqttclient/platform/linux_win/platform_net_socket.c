﻿/*
 * @Author: jiejie
 * @Github: https://github.com/jiejieTop
 * @Date: 2020-01-10 23:45:59
 * @LastEditTime: 2020-06-05 17:13:00
 * @Description: the code belongs to jiejie, please keep the author information and source code according to the license.
 */
#include "platform_net_socket.h"
#include "mqtt_error.h"

#ifdef _WIN32

#pragma comment(lib, "ws2_32.lib")

static int platform_winsock_initialized = 0;

int platform_init_winsock() {
    if (0 == platform_winsock_initialized) {
        WSADATA wsaData;
        int res = WSAStartup(MAKEWORD(2, 2), &wsaData);
        if (res != 0) {
            printf("WSAStartup failed with error: %d\n", res);
            return 1;
        }
        platform_winsock_initialized = 1;
    } else if (0 < platform_winsock_initialized) {
        platform_winsock_initialized++;
    }
    return 0;
}

int platform_cleanup_winsock() {
    if (1 == platform_winsock_initialized) {
        WSACleanup();
        platform_winsock_initialized = 0;
    } else if (1 < platform_winsock_initialized) {
        platform_winsock_initialized--;
    }
    return 0;
}

#ifndef PIKA_PLATFORM_NO_WEAK

void  usleep(unsigned long usec){
    HANDLE timer;
    LARGE_INTEGER interval;
    interval.QuadPart = (10 * usec);
    timer = CreateWaitableTimer(NULL, TRUE, NULL);
    SetWaitableTimer(timer, &interval, 0, NULL, NULL, 0);
    WaitForSingleObject(timer, INFINITE);
    CloseHandle(timer);
}

int gettimeofday(struct timeval *tp, void *tzp){
  time_t clock;
  struct tm tm;
  SYSTEMTIME wtm;
  GetLocalTime(&wtm);
  tm.tm_year   = wtm.wYear - 1900;
  tm.tm_mon   = wtm.wMonth - 1;
  tm.tm_mday   = wtm.wDay;
  tm.tm_hour   = wtm.wHour;
  tm.tm_min   = wtm.wMinute;
  tm.tm_sec   = wtm.wSecond;
  tm. tm_isdst  = -1;
  clock = mktime(&tm);
  tp->tv_sec = clock;
  tp->tv_usec = wtm.wMilliseconds * 1000;
  return (0);
}

void timeradd(struct timeval *a, struct timeval *b, struct timeval *res){
    res->tv_sec = a->tv_sec + b->tv_sec;
    res->tv_usec = a->tv_usec + b->tv_usec;
    if (res->tv_usec >= 1000000) {
        res->tv_sec += res->tv_usec / 1000000;
        res->tv_usec %= 1000000;
    }
}

void timersub(struct timeval *a, struct timeval *b, struct timeval *res){
    res->tv_sec = a->tv_sec - b->tv_sec;
    res->tv_usec = a->tv_usec - b->tv_usec;
    if (res->tv_usec < 0) {
        res->tv_sec -= 1;
        res->tv_usec += 1000000;
    }
}
#endif

#endif

int platform_fcntl(int fd, int cmd, long arg) {
#if defined(__linux__)
    return fcntl(fd, cmd, arg);
#elif defined(_WIN32)
    if (cmd == F_GETFL) {
        u_long mode = 0;
        ioctlsocket(fd, FIONBIO, &mode);
        return (mode ? O_NONBLOCK : 0);
    } else if (cmd == F_SETFL) {
        u_long mode = (arg & O_NONBLOCK) ? 1 : 0;
        return ioctlsocket(fd, FIONBIO, &mode);
    }
    return -1;
#endif
}

/* os file API */
int platform_close(int __fd) {
#if defined(__linux__)
    return close(__fd);
#elif defined(_WIN32)
    return closesocket(__fd);
#endif
}

int platform_net_socket_connect(const char *host, const char *port, int proto)
{
    int fd, ret = MQTT_SOCKET_UNKNOWN_HOST_ERROR;
    struct addrinfo hints, *addr_list, *cur;
    
    /* Do name resolution with both IPv6 and IPv4 */
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = (proto == PLATFORM_NET_PROTO_UDP) ? SOCK_DGRAM : SOCK_STREAM;
    hints.ai_protocol = (proto == PLATFORM_NET_PROTO_UDP) ? IPPROTO_UDP : IPPROTO_TCP;
    
    if (getaddrinfo(host, port, &hints, &addr_list) != 0) {
        return ret;
    }
    
    for (cur = addr_list; cur != NULL; cur = cur->ai_next) {
        fd = socket(cur->ai_family, cur->ai_socktype, cur->ai_protocol);
        if (fd < 0) {
            ret = MQTT_SOCKET_FAILED_ERROR;
            continue;
        }

        if (connect(fd, cur->ai_addr, cur->ai_addrlen) == 0) {
            ret = fd;
            break;
        }

        close(fd);
        ret = MQTT_CONNECT_FAILED_ERROR;
    }

    freeaddrinfo(addr_list);
    return ret;
}

int platform_net_socket_recv(int fd, void *buf, size_t len, int flags)
{
    return recv(fd, buf, len, flags);
}

int platform_net_socket_recv_timeout(int fd, unsigned char *buf, int len, int timeout)
{
    int nread;
    int nleft = len;
    unsigned char *ptr; 
    ptr = buf;

    struct timeval tv = {
        timeout / 1000, 
        (timeout % 1000) * 1000
    };
    
    if (tv.tv_sec < 0 || (tv.tv_sec == 0 && tv.tv_usec <= 0)) {
        tv.tv_sec = 0;
        tv.tv_usec = 100;
    }

    platform_net_socket_setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(struct timeval));

    while (nleft > 0) {
        nread = platform_net_socket_recv(fd, ptr, nleft, 0);
        if (nread < 0) {
            return -1;
        } else if (nread == 0) {
            break;
        }

        nleft -= nread;
        ptr += nread;
    }
    return len - nleft;
}

int platform_net_socket_write(int fd, void *buf, size_t len)
{
#ifdef __linux
    return write(fd, buf, len);
#elif defined(_WIN32)
    return send(fd, buf, len, 0);
#endif
}

int platform_net_socket_write_timeout(int fd, unsigned char *buf, int len, int timeout)
{
    struct timeval tv = {
        timeout / 1000, 
        (timeout % 1000) * 1000
    };
    
    if (tv.tv_sec < 0 || (tv.tv_sec == 0 && tv.tv_usec <= 0)) {
        tv.tv_sec = 0;
        tv.tv_usec = 100;
    }

    setsockopt(fd, SOL_SOCKET, SO_SNDTIMEO, (char *)&tv,sizeof(struct timeval));
    
    return platform_net_socket_write(fd, buf, len);
}

int platform_net_socket_close(int fd)
{
    return platform_close(fd);
}

int platform_net_socket_set_block(int fd)
{
    return platform_fcntl(fd, F_SETFL, platform_fcntl(fd, F_GETFL, F_GETFL) & ~O_NONBLOCK);
}

int platform_net_socket_set_nonblock(int fd)
{
    return platform_fcntl(fd, F_SETFL, platform_fcntl(fd, F_GETFL, F_GETFL) | O_NONBLOCK);
}

int platform_net_socket_setsockopt(int fd, int level, int optname, const void *optval, socklen_t optlen)
{
    return setsockopt(fd, level, optname, optval, optlen);
}

