QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += _CRT_NONSTDC_NO_DEPRECATE
DEFINES += _CRT_SECURE_NO_WARNINGS
DEFINES += HAVE_STRUCT_TIMESPEC
#win32-msvc* {
#DEFINES += _WINSOCK_DEPRECATED_NO_WARNINGS
#}

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
DEFINES += PIKA_CONFIG_ENABLE
DEFINES += PIKA_PLATFORM_NO_WEAK


SOURCES += \
    PikaPlatform_win.c \
    main.c \

HEADERS += \
    pika_config.h

HEADERS += $$files(pikapython/*.h, true)
HEADERS += $$files(pthread/*.h, true)
HEADERS += $$files(mqttclient/common/*.h, true)
HEADERS += $$files(mqttclient/config/*.h, true)
HEADERS += $$files(mqttclient/mqtt/*.h, true)
HEADERS += $$files(mqttclient/mqttclient/*.h, true)
HEADERS += $$files(mqttclient/network/*.h, true)
HEADERS += $$files(mqttclient/platform/linux_win/*.h, true)

SOURCES += $$files(pikapython/*.c, true)

SOURCES += $$files(mqttclient/common/log/arch/linux_win/*.c, true)
SOURCES += $$files(mqttclient/common/log/fifo.c, true)
SOURCES += $$files(mqttclient/common/log/format.c, true)
SOURCES += $$files(mqttclient/common/log/salof.c, true)
SOURCES += $$files(mqttclient/common/mqtt_list.c, true)
SOURCES += $$files(mqttclient/common/mqtt_random.c, true)

SOURCES += $$files(mqttclient/mqtt/*.c, true)
SOURCES += $$files(mqttclient/mqttclient/*.c, true)
SOURCES += $$files(mqttclient/network/*.c, true)
SOURCES += $$files(mqttclient/platform/linux_win/*.c, true)

OTHER_FILES += $$files(pikapython/*.py, true)
OTHER_FILES += $$files(pikapython/*.pyi, true)
OTHER_FILES += $$files(pikapython/requestment.txt, true)

# 添加头文件搜索路径
INCLUDEPATH +=  $$PWD/pthread \
                $$PWD/pikapython/pikascript-core \
                $$PWD/pikapython/pikascript-api  \
                $$PWD/pikapython/pikascript-lib/PikaStdLib  \
                $$PWD/pikapython/pikascript-lib/socket  \
                $$PWD/pikapython/pikascript-lib/json  \
                $$PWD/mqttclient/common \
                $$PWD/mqttclient/common/log \
                $$PWD/mqttclient/config \
                $$PWD/mqttclient/mqtt \
                $$PWD/mqttclient/mqttclient \
                $$PWD/mqttclient/network \
                $$PWD/mqttclient/network/mbedtls/include \
                $$PWD/mqttclient/network/mbedtls/include/mbedtls \
                $$PWD/mqttclient/network/mbedtls/wrapper \
                $$PWD/mqttclient/platform/linux_win \


# 当添加PikaMath的时候需要添加连接math.h
# LIBS += -lm

# 添加pthread库链接
# LIBS += -lpthread

# MSVC Compiler - Disable warning C4244
win32-msvc* {
    QMAKE_CFLAGS += /wd4003
    QMAKE_CFLAGS += /wd4018
    QMAKE_CFLAGS += /wd4113
    QMAKE_CFLAGS += /wd4200
    QMAKE_CFLAGS += /wd4244
    QMAKE_CFLAGS += /wd4267
    QMAKE_CFLAGS += /wd4273
    QMAKE_CFLAGS += /wd4819
}

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32: LIBS += -L$$PWD/./ -lpthreadVC2
win32: LIBS += -L$$PWD/./ -lpthreadVCE2
win32: LIBS += -L$$PWD/./ -lpthreadVSE2

INCLUDEPATH += $$PWD/pthread
DEPENDPATH += $$PWD/pthread
