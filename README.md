

# pikapython-msvc-qt

## 介绍

本工程为pikapython在qt中的移植，编译环境基于MSVC。并且移植pthread到qt当中，添加了PIKA_PLATFORM_NO_WEAK的宏定义的支持，让pikapython的多线程支持在windows平台下。并且添加了PikaStdLib，math，socket, _thread, radom, time, json, aes, base64, mqttclient（mqttclient库是移植mqttclient库到msvc，另外pika接口部分参考pika的mqtt，因为直接采用mqtt会和socket冲突无法编译）等库的导入，可以运行基本的pikapython多线程代码，支持socket通信。

### 编译环境

QT版本：			5.14.2

开发环境：	Qt Creator 4.11.1 / Visual Studio 2022

编译环境：		MSVC2017_32bit

> 注意：
>
> ​			(1) 、 用QT打开文件，只需要确保安装对应版本的QT（5.14.2），以及开发环境（Qt Creator 4.11.1），和对应的编译器（MSVC2017_32bit）。打开pikapython.pro工程文件编译即可。
>
> 
>
> ​			(2)、 用Visual Studio 2022打开工程，请先确保电脑系统安装了对应版本的QT和相应的编译器MSVC2017_32bit。另外，在Visual Studio 2022中需要安装扩展（Qt Visual Studio Tools）；然后在”扩展“ --> ”QT for Tools“ --> "Options"，在选项卡的QT下的Version选项，添加对应的编译器，如下图。然后打开pikapython.sln文件即可编译。
>
> ![qt_visualstudio_tools](./img/qt_visualstudio_tools.png)

## 需要注意

1. 如果添加了其他库文件，重新运行工程目录/python目录下的“pikaPackage.exe”和“rust-msc-latest-win10.exe”可能会导致编译错误，请修改报错的文件的尾行序列从“LF”为windows的尾行标志“CRLF”。

   ![CRLF](./img/CRLF.png)

   

2. 编译完成之后需要运行的，请先将pthreadVC2.dll，pthreadVCE2.dll，pthreadVSE2.dll复制到编译后的exe所在的目录下运行，否则可能会由于缺少库导致无法运行。

3. 如果需要运行math，time相关的函数，请将math.py，time.py复制到你需要运行的.py文件目录下。否则运行可能出错，提示编译时找不到对应的.py文件（测试时.pyi可复制可不复制，不复制会弹出警告，但是不影响运行）。



## 测试结果

### 1、 直接运行pikapython.exe

![01](./img/01.png)



### 2、运行math相关函数

a.py的内容：

```python
import PikaStdLib
import math


def procedure(x):
    ssfd = x
    print(ssfd)
    return ssfd



a = math.sin(2)
print("Hello")
print(a)
print("LLLLL")
print("Hello")
mm = procedure(1 + 1)

```

运行结果：

**![02](./img/02.png)**



### 3、运行thread多线程相关任务

test1.py的内容：

```python
import _thread
import time

task1_finished = False
task2_finished = False


def task1():
    global task1_finished
    print("task1")
    for i in range(10):
        time.sleep(0.05)
        print("task1")
    task1_finished = True


def task2(sleep_time, loop_count):
    global task2_finished
    print("task2:", sleep_time, loop_count)
    for i in range(loop_count):
        time.sleep(sleep_time)
        print("task2")
    task2_finished = True


_thread.start_new_thread(task1, ())
_thread.start_new_thread(task2, (0.05, 10))

while not task1_finished or not task2_finished:
    time.sleep(0.1)

time.sleep(0.5)  # wait for threads to exit

```

运行结果：

![03](./img/03.png)

### 4、运行socket相关任务

socket_thread.py代码如下：

```python
import socket
import _thread
import random
import time

test_finished = False
server_started = False


def socket_server_task(host, port):
    """
    socket 服务器任务
    :return:
    """
    print("socket server start:", host, port)
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((host, port))
    s.listen(5)
    print("socket server waiting accept")
    global server_started
    server_started = True
    accept, addr = s.accept()
    print("socket server accepted at", addr)
    while True:
        try:
            data = accept.recv(1024)
            print('socket server recv:', data.decode())
            accept.send(data)
        except Exception:
            print('socket server closing accept')
            accept.close()
            break
    print("socket server closing")
    s.close()
    global test_finished
    test_finished = True


def socket_server_init(host='127.0.0.1', port=36500):
    _thread.start_new_thread(socket_server_task, (host, port))


def socket_client_task(host, port):
    print("socket client start:", host, port)
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect((host, port))
    client.send("hello".encode())
    recv = client.recv(1024).decode()
    print("client recv:", recv)
    client.close()


def socket_server_test(host='127.0.0.1', port=36500):
    _thread.start_new_thread(socket_client_task, (host, port))


test_port = random.randint(10000, 65535)
socket_server_init(port=test_port)
while not server_started:
    time.sleep(0.1)
socket_server_test(port=test_port)
while not test_finished:
    time.sleep(0.1)

```

运行结果如下：

![socket_thread](./img/socket_thread.png)

### 4、运行mqtt相关程序

1. 这里用emqx创建一个mqtt Broker（只是测试，用的是开源windows版）：[https://www.emqx.com/zh/try?product=broker](https://www.emqx.com/zh/try?product=broker) ，下载解压，进入bin目录：直接输入```emqx start```就可以启动了。
2. 通过浏览器访问 http://localhost:18083 （localhost 可替换为您的实际 IP 地址，比如：127.0.0.1）以访问 [EMQX Dashboard](https://www.emqx.io/docs/zh/latest/dashboard/introduction.html) 管理控制台，进行设备连接与相关指标监控管理。
3. 下载mqtt客户端MQTTX(也可以直接用Web端则不用下载)，下载和运行地址：[https://mqttx.app/zh](https://mqttx.app/zh) 。 启动后，连接Broker，添加一个订阅，这里订阅节点，这里设置的节点为```testtopic/de```
4. 运行以下发布者代码。订阅者的代码可以参考pika的example代码，修改IP和topic即可，本人测试过，可以运行

```python
import mqttclient

client = mqttclient.MQTT('127.0.0.1')

client.setHost('127.0.0.1')
client.setPort(1883)
client.setClientID('123456dddecetdc')
client.setUsername('test1')
client.setPassword('aabbccdd')
client.setVersion('3.1')
client.setKeepAlive(10)

ret = client.connect()
print("connect ret:%d" % ret)

client.publish('testtopic/de', 'hello pikascript qos=0', 0)
client.publish('testtopic/de', 'hello pikascript qos=1', 1)
client.publish('testtopic/de', 'hello pikascript qos=2', 2)

ret = client.disconnect()
print("disconnect ret:%d" % ret)

```

运行代码，可以看到mqttx客户端已经打印出pikapython端发布的信息

![mqttx_sub](img/mqttx_sub.png)

## 感谢以下项目作者

1.  [PikaPython: https://github.com/pikasTech/PikaPython](https://github.com/pikasTech/PikaPython)
2.  [mqttclient: https://github.com/jiejieTop/mqttclient](https://github.com/jiejieTop/mqttclient)

